<?php

namespace App\Nova;

use App\Nova\Filters\Corrected;
use App\Nova\Filters\Corrector;
use App\Nova\Filters\Judged;
use App\User;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;

class Entry extends Resource
{
	private $fullDetailsUsers = [1, 8, 9, 18];

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Entry';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
		'title',
		'title_edited'
    ];

    public function title()
	{
		return $this->id . ': ' . $this->title;
	}

	public function corrector() {
		$numberOfCorrectors = 5;

		// map Modulo of entry ID to user ID
		$correctorMap = [
			0 => 3,
			1 => 4,
			2 => 5,
			3 => 6,
			4 => 7,
		];

		return User::find($correctorMap[$this->id % $numberOfCorrectors]);
	}


	/**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

			Text::make('Corrector')->displayUsing(function() {
				return $this->corrector()->name;
			})->onlyOnIndex(),

			Text::make('Original title', 'title')->hideFromIndex()->readonly(),

			Textarea::make('Original story', 'story')->alwaysShow()->readonly(),

			Text::make('Title edited')->asHtml(),

			Textarea::make('Story edited'),

			Textarea::make('Notes')->alwaysShow()->readonly(),

			Text::make('Entrant')->displayUsing(function() {
				return $this->full_name . '<br>' . $this->email;
			})->asHtml()->canSee(function () {
				return in_array(auth()->user()->id, $this->fullDetailsUsers);
			}),

			BelongsTo::make('School')->canSee(function () {
				return in_array(auth()->user()->id, $this->fullDetailsUsers);
			}),

			BelongsTo::make('Year', 'schoolYear', 'App\Nova\SchoolYear'),

			Boolean::make('Published', 'is_published')->help('This story is okay to publish on the website')->hideFromIndex(),

			Boolean::make('Checked', 'is_corrected'),

			Boolean::make('Judged', 'is_judged'),

		];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
        	new \App\Nova\Filters\SchoolYear(),
        	new \App\Nova\Filters\School(),
        	new Corrected(),
			new Judged(),
			new Corrector(),
		];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
