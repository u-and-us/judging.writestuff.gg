<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

class School extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
	public function apply(Request $request, $query, $value)
	{
		if ($value) {
			return $query->whereHas('school', function($query) use ($value) {
				$query->where('id', '=', $value);
			});
		}
	}

	/**
	 * Get the filter's available options.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function options(Request $request)
	{
		return \App\School::select(['id', 'name'])->orderBy('name')->get()->keyBy('name')->transform(function ($item) {
			return $item->id;
		})->toArray();
	}
}
