<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

class SchoolYear extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
		if ($value) {
			return $query->whereHas('schoolyear', function($query) use ($value) {
				$query->where('id', '=', $value);
			});
		}
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
		return \App\SchoolYear::select(['id', 'year'])->orderBy('year')->get()->keyBy('year')->transform(function ($item) {
			return $item->id;
		})->toArray();
    }
}
