<?php

namespace App\Nova\Filters;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

class Corrector extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
		$correctorMap = [
			0 => 3,
			1 => 4,
			2 => 5,
			3 => 6,
			4 => 7,
		];

		if ($value) {
			return $query->whereRaw('MOD(id, 5) = ' . array_flip($correctorMap)[$value]);
		}
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
		$correctorMap = [
			0 => 3,
			1 => 4,
			2 => 5,
			3 => 6,
			4 => 7,
		];

		return \App\User::select(['id', 'name'])->whereIn('id', $correctorMap)->orderBy('name')->get()->keyBy('name')->transform(function ($item) {
			return $item->id;
		})->toArray();
    }
}
