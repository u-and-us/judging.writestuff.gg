<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Entry extends Model
{
	use SoftDeletes;

	protected $guarded = [];

    public function school() {
    	return $this->belongsTo(School::class);
	}

    public function schoolYear() {
    	return $this->belongsTo(SchoolYear::class);
	}

    public function correctorName() {
    	$numberOfCorrectors = 5;

		// map Modulo of entry ID to user ID
    	$correctorMap = [
    		0 => 3,
    		1 => 4,
    		2 => 5,
    		3 => 6,
    		4 => 7,
		];

		return User::find($correctorMap[$this->id % $numberOfCorrectors])->name;
    }

	public function getFullNameAttribute() {
    	return $this->name . ' ' . $this->surname;
	}

	public function getTitleEditedAttribute($value)
	{
		return $value ?? $this->title;
	}

	public function getStoryEditedAttribute($value)
	{
		return $value ?? $this->story;
	}
}
